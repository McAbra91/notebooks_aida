## Coronal Holes Segmentation using UNet

### Preparation for running inference engine
Please download dataset in order to run the notebook properly. You can find the available data [here](https://osf.io/48jyb/files/). There are 2 choices, which contains different amount of images to inference on:

1) ***CoronalHoles.zip***: 300 images
2) ***CoronalHoles_test.zip***: 28 images

Download the ```.zip``` file and extract in inside the notebook`s folder. The appropriate folder structure should be like the example below:

    -ch_segmentation
        |
        -- ch_notebook.ipynd
        -- model_best.pt
        -- CoronalHoles
                |
                - IMGS
                - MASKS


Moreover, please download the ChoronalHoles Unet Segmentator weights **model_best.pt** and add it into notebook`s master folder.

The available dataset contains 300 images and their corresponding masks. You can select any subset of your choice and add it with the previous folder architecture.


###### Make sure that your Python environment contains all the necessary packages.